var express = require('express');
var http = require('http');

var web_app = express();
var web_server = http.createServer(web_app);

web_app.get('/', function(request, response) {
	response.sendFile(__dirname + "/index.html");
	//response.send('Hello World');
});

web_server.listen(8080, function() {
	console.log("The server is now up and running...listening on port %d", web_server.address().port);
});