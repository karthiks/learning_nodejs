var express = require('express');
var http = require('http');
var url = require('url');

var app = express();
var ws = http.createServer(app);

app.get('/tweets/:username', function(req,res) {
	var username = req.params.username;

	var options = {
		protocol : 'https:',
		host : 'api.twitter.com',
		pathname : '1.1/statuses/home_timeline.json',
		query : { screen_name: username, count:10}
	};

	var twitter_url = url.format(options); //converts options json to string
	req(twitter_url).pipe(res); //pipe the request to response
});

ws.listen(8080, function() {
	console.log("The server is now up and running at port 8080");
});


//-------------- FAIL -----------------------------------------------------------------------------------
	This code would not work because Twitter v1.1 requires authentication which needs to be done..
//-------------- FAIL -----------------------------------------------------------------------------------

//$ curl -s http://localhost:8080/tweets/kartzontech | prettyjson
//prettyjson above is a node executable that can be installed globally as below:
//$ npm install prettyjson -g