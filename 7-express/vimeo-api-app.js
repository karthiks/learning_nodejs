var express = require('express');
var http = require('http');
var url = require('url');
var http_request = require('request'); //Note this request is different from the param in callback function in app.get(..)

var app = express();
var ws = http.createServer(app);

app.get('/vimeo/:username/:action', function(req, res) {

	var username = req.params.username;
	var action = req.params.action;

	//https://developer.vimeo.com/apis/simple#user-request-methods
	var options = {
		protocol: 'http:',
		host: 'vimeo.com',
		pathname: '/api/v2/' + username + '/' + action + '.json'
	};

	var vimeo_url = url.format(options);
	console.log(vimeo_url);
	http_request(vimeo_url).pipe(res);
	// res.end(vimeo_url); //for quick-debugging purposes
});

ws.listen(8080, function() {
	console.log("The server is now up and running.. and listening at port %d", ws.address().port);
});

//To test this app start the server:
// $ node vimeo-api-app.js

//To test the app run the following in your other terminal
// $ curl http://localhost:8080/vimeo/user4884323/info | prettyjson
// $ curl http://localhost:8080/vimeo/user4884323/videos | prettyjson
// $ curl http://localhost:8080/vimeo/user4884323/appears_in | prettyjson
// $ curl http://localhost:8080/vimeo/user4884323/all_videos | prettyjson
// $ curl http://localhost:8080/vimeo/user4884323/subscriptions | prettyjson
// $ curl http://localhost:8080/vimeo/user4884323/albums | prettyjson
// $ curl http://localhost:8080/vimeo/user4884323/channels | prettyjson
// $ curl http://localhost:8080/vimeo/user4884323/groups | prettyjson