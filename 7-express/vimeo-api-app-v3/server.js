var app = require('./vimeo-api-app-v3.js');
var http = require('http');
var ws = http.createServer(app);

ws.listen(8080, function() {
	console.log("The server is now up and running.. and listening at port %d", ws.address().port);
});

//To test this app do the following:
// $ cd 7-express
// $ node server

//To test the app run the following in your other terminal
// $ curl http://localhost:8080/vimeo/user4884323/info | prettyjson
// $ curl http://localhost:8080/vimeo/user4884323/videos | prettyjson
// $ curl http://localhost:8080/vimeo/user4884323/appears_in | prettyjson
// $ curl http://localhost:8080/vimeo/user4884323/all_videos | prettyjson
// $ curl http://localhost:8080/vimeo/user4884323/subscriptions | prettyjson
// $ curl http://localhost:8080/vimeo/user4884323/albums | prettyjson
// $ curl http://localhost:8080/vimeo/user4884323/channels | prettyjson
// $ curl http://localhost:8080/vimeo/user4884323/groups | prettyjson