var express = require('express');
var url = require('url');
var http_request = require('request'); //Note this request is different from the param in callback function in app.get(..)
var ejs = require('ejs');

var app = express();

app.get('/', function(req, res) {
	res.send("The url path should be /vimeo/[your_user_name]");
});

app.get('/vimeo/:username', function(req, res) {

	var username = req.params.username;

	//https://developer.vimeo.com/apis/simple#user-request-methods
	var options = {
		protocol: 'http:',
		host: 'vimeo.com',
		pathname: '/api/v2/' + username + '/likes.json'
	};

	var vimeo_url = url.format(options);
	console.log(vimeo_url);
	// http_request(vimeo_url).pipe(res);
	http_request(vimeo_url, function(err, response, body) {
		var items = JSON.parse(body);
		// res.render('../vimeo-output.ejs', {likes: items, username: username});
		res.send(items);
	});
});

module.exports = app;