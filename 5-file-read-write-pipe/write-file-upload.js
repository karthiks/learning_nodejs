var fs = require('fs');
var http = require('http');

var web_server = http.createServer().listen(8080);
web_server.on('request', function(request, response) {
	var newfile = fs.createWriteStream("upload-text-copy.txt");
	request.pipe(newfile);

	request.on('end', function() {
		response.end("uploaded");
	});
} );

console.log("Server up and running now..");
console.log("Listening on port 8080");

//To start server in terminal
//$ node write-file-upload.js
//To upload a file from terminal
//$ curl --upload-file random-text-copy.txt http://localhost:8080