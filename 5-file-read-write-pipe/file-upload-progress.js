var fs = require('fs');
var http = require('http');

http.createServer( function(request, response) {
	var new_file = fs.createWriteStream("file-upload-progress-"+ Date.now() + ".pdf");
	console.log(request.headers);
	var file_byte_length = request.headers['content-length'];
	var uploaded_bytes = 0;

	request.pipe(new_file);

	request.on('data', function(chunk) {
		uploaded_bytes += chunk.length;
		var progress = (uploaded_bytes / file_byte_length) * 100;
		response.write("Progress: " + parseInt(progress, 10) + "%\n");
	});

	request.on('end', function() {
		response.end("uploaded and done!..");
	});
}).listen(8080);

console.log("The server is now up and running..listening at port:8080")

// To test this app run the server 
// $ node file-upload-progress.js

// and run the following command in another terminal
// $ curl --upload-file farewell-nodejs.pdf http://localhost:8080