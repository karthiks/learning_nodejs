var http = require('http');

var make_request = function(message) {
	var options = {
		host : 'localhost',
		port : 8080,
		path : '/',
		method : 'POST'
	};

	var request = http.request(options, function(response) {
		response.on('data', function(data) {
			console.log(data.toString());
		});
	});

	request.write(message);
	request.end();
};

module.exports = make_request;