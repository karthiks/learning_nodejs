var http = require('http');

var ws = http.createServer();

ws.on('request', function(request, response) {
	request.pipe(response);

	// request.on('data', function(chunk) {
	// 	response.writeHead(200);
	// 	response.write("The server received the data: \n-------------------------------------------------\n");
	// 	//response.write(chunk);
	// 	console.log(chunk.toString());
	// });
	
	request.on('end', function() {
		response.end("\n-------------------------------------------------");
	});
} );

ws.listen(8080);
console.log("The server is now up and running at port %d", ws.address().port);

//Either curl from terminal as below
curl --data "param1=value1&param2=value2" http://localhost:8080
//Or alternatively, run the make_request_client.js  //<----DO-THIS