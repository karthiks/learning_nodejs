//START:------------MONGOOSE------------------
	var mongoose = require('mongoose');
	mongoose.connect('mongodb://localhost/psicologosTuxtepecDB');

	var Schema = mongoose.Schema;
	var userCredential = new Schema({
	    username: String,
	    password: String
	}, {
	    collection: 'members'
	});

	var userCredentials = mongoose.model('members', userCredential);
//END:------------MONGOOSE--------------------

exports.passport = function() {
	var passport = require('passport');
	passport.serializeUser(function(user, done) {
	    done(null, user);
	});

	passport.deserializeUser(function(user, done) {
	    done(null, user);
	});

	passport.use(new LocalStrategy(function(username, password, done) {
	    process.nextTick(function() {
	        userCredentials.findOne({
	            'username': username,
	        }, function(err, user) {
	            if (err) {
	                return done(err);
	            }

	            if (!user) {
	                return done(null, false);
	            }

	            if (user.password != password) {
	                return done(null, false);
	            }

	            return done(null, user);
	        });
	    });
	}));
};