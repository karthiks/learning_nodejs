	var Server = require('mongodb').Server;
	var BSON = require('mongodb').BSONPure;
	var Db = require('mongodb').Db;
	var db = new Db('psicologosTuxtepecDB', new Server('localhost', 27017), {safe:false});

    // Select a collection
    var psicologosCollection = null;

    exports.save = function(request, response) {
        var psychologist = {
            personalData: request.body._personalData,
            professionalData: request.body._professionalData,
            professionalInterests: request.body._professionalInterests
        };

	    db.open(function(err, db) {
	    	psicologosCollection = db.collection("psicologos");
	    });

        psicologosCollection.insert(psychologist, function(error, responseFromDB) {
            if (error) {
                response.send(responseFromDB)
            }
            console.log("It has been inserted: " + JSON.strinfigy(responseFromDB))
            response.send(responseFromDB)
        });
    };

    exports.findOne = function(request, response) {
        var id = new BSON.ObjectID(peticion.params.id);
	    db.open(function(err, db) {
	    	psicologosCollection = db.collection("psicologos");
	    });

        psicologosCollection.findOne({
                '_id': id
            },
            function(error, responseFromDB) {
                if (error) {
                    response.send(responseFromDB)
                }
                response.send(responseFromDB)
            })
    };

    exports.list = function(request, response) {
	    db.open(function(err, db) {
	    	psicologosCollection = db.collection("psicologos");
	    });

        psicologosCollection.find().toArray(function(error, responseFromDB) {
            if (error) {
                response.send(responseFromDB)
            }
            response.send(responseFromDB)
        })
    };
