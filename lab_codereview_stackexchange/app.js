module.exports = function() {
	var express = require('express');
	var bodyParser = require('body-parser');
	var LocalStrategy = require('passport-local').Strategy;

	var app = express();

	var passport = require('./auth');
	app.use(passport.initialize());
	app.use(passport.session());

	app.use(express.static(__dirname + "/public"));
	// app.use(bodyParser()); //Now deprecated
	app.use(bodyParser.urlencoded({
	    extended: true
	}));
	app.use(bodyParser.json());

    app.get('/registro', function(request, response) {
        response.sendfile("public/html/registro.html");
    });

    var psychos = require('./psychos');
    // When we make a HTTP POST request to the psychologists route ...
    app.post("/psychos", psychos.save);

    app.get("/psychos/:id", psychos.findOne);

    app.get("/psychos", psychos.list);

    app.post('/login',
        passport.authenticate('local', {
            successRedirect: '/loginSuccess',
            failureRedirect: '/loginFailure'
        })
    );

    app.get('/loginFailure', function(req, res, next) {
        res.redirect('/')
    });

    app.get('registro', function(request, response) {
        response.sendfile('public/html/registro.html')
    });

    app.get('/loginSuccess', function(req, res, next) {
        res.redirect('/registro')
    });

};