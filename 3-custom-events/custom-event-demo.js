var EventEmitter = require('events').EventEmitter;

var logger = new EventEmitter();

//Listen for the error event
logger.on('error', function(message){
	console.log("ERR: " + message);
} );

//To trigger or emit the event we call emit() on logger
logger.emit('error', "This is test error emit event log..");
logger.emit('error', "Another error event emitted");