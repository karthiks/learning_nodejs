var fs = require("fs");

console.log("Let's do the file reading..");

fs.readFile("random-texts.txt", callbackFunction); //This should throw an error :(
fs.readFile("random-text.txt", callbackFunction);  //This should o/p file contents :)

console.log("..");
console.log("..");
console.log("There is more that can be done parallel while reading stuff to the file..if only the reading is non-blocking!");
console.log("..");
console.log("..");

function callbackFunction(error, content) {
	if(error != undefined) {
		console.log("Oops, something has gone awry!..");
		console.log(error.toString());
		return;
	}
	console.log("Done reading file..");
	console.log("File contents: ");
	console.log(content.toString());
};