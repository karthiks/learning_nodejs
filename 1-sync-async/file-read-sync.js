var fs = require("fs");

console.log("Let's do the file reading..");

var content = fs.readFileSync("random-text.txt");
console.log("File contents: ");
console.log(content.toString());

console.log("Done reading file..");