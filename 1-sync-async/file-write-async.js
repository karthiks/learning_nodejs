var fs = require("fs");

console.log("Let's do the file writing..");

var content = "If you see this text in the file then the file write is success. --Karthik";
fs.writeFile("test-file.out", content, function(error) {
	if(error != undefined) {
		console.log(error.toString());
	}
	console.log("Successfully done with file writing..");
});

console.log("..");
console.log("..");
console.log("There is more that can be done parallel while writing stuff to the file..if only the writing is non-blocking!");
console.log("..");
console.log("..");