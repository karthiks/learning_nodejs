var http = require('http');

//http.createServer() returns a new Web Server object http.Server
var web_server = http.createServer();

//http.Server is an EventEmitter with 'request' being one of the emitters
web_server.on( 'request', function(request, response) {
	response.writeHead(200);
	response.write("Hi there!");
	response.write("I'm gonna send chunks of data as stream..howz that?");
	setTimeout( function() {
		//This timeout function represents long running process
		response.write("Hi again!!..");
		response.end();
	}, 5000 ); //5 secs timeout
} );

web_server.listen(8080)

console.log("Server up and running..");
console.log("Listening on port 8080..");