var http = require('http');

http.createServer( function(request, response) {
	response.writeHead(200);
	request.pipe(response);
} ).listen(8080);

console.log("Server up and running..");
console.log("Listening on port 8080..");

//To run this Server from terminal
//$ node echo-server-v2.js

//To see the output from this Server
//$ curl --data "param1=value1&param2=value2" http://localhost:8080
//$ curl -d "hello!" http://localhost:8080