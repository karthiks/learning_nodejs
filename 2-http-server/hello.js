var http = require('http');

http.createServer( function(request, response) {
	response.writeHead(200);
	response.write("Hello there!");
	response.end();
} ).listen(8080);

console.log("Server up and running..");
console.log("Listening on port 8080..");

//To run this Server from terminal
//$ node hello.js

//To see the output from this Server
//$ curl http://localhost:8080/