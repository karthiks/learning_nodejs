exports = module.exports = {};

exports.parse = function(args, defaults) {
  var options = {};
  if ( (typeof defaults === "object") && !(defaults instanceof Array)) {
    options = defaults
  };

  args.forEach(function(item, intex){
    //Check if Long formed tag
    if(item.substr(0,2) === "--") {
      item = item.substr(2); //Discard the "--" prefix

      if (item.indexOf("=") !== -1) {
        item = item.split("=");
        var key = item.shift();
        var value = item.join("=");

        if(/^[0-9]+$/.test(value)) {
        // if( ! isNaN(value) ) {  
          value = parseInt(value, 10);
        };
        options[key] = value;
      }
      else {
        options[item] = true;
      }
    }
  });
  return options;
};