var express = require('express');
var http = require('http');
var socket = require('socket.io');

var app = express();
var ws = http.createServer(app);
var io = socket(ws);

app.get('/', function(req,res) {
	res.sendFile(__dirname + '/index.html');
});

io.on('connection', function(client) {
	console.log('a user connected..');

	//emit the messages event onto the client
	client.emit('messages', {hi: "Hello world!"});

	client.on('disconnect', function(){
    	console.log('user disconnected');
  	});
});

ws.listen(8080, function() {
	console.log("The server is now up and running.. listening at port %d", ws.address().port);
});