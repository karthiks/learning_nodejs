var express = require('express');
var http = require('http');
var socket = require('socket.io');
var _ = require('lodash');

var app = express();
var ws = http.createServer(app);
var io = socket(ws);

app.get('/', function(req,res) {
	res.redirect('/chat');
});

app.get('/chat', function(req,res) {
	res.sendFile(__dirname + '/chat-room.html');
});

ws.listen(8080, function(){
	console.log("The server is now up and running.. and listening at port 8080");
});

var client_names = [];
io.on('connection', function(client){

	client.on('join', function(name) {
		client.emit('existing-chatters', {existing_chatters: client_names});
		console.log("Adding "+ name + " to chat-room..")
		client.name = name;
		client_names.push(name);
		console.log("New clients list : " + client_names);

		client.broadcast.emit('add-chatter',name);
	});

	client.on('disconnect', function(){
		console.log(client.name + " has left the chat-room");
		_.remove(client_names, function(cn){
			return cn === client.name;
		});
		client.broadcast.emit('remove-chatter',client.name);
		console.log("New clients list : " + client_names);
	});

	client.on('send-message', function(message){
		console.log(client.name + ": " + message);
		client.broadcast.emit('get-message', {sender: client.name, message: message});
	});

});