function foo() {
	console.log("logging foo..");
}

function bar() {
	console.log("logging bar..");
}

exports.foo = foo;
exports.bar = bar;