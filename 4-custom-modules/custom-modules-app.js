// require('./custom_goodbye').goodbye(); if only we need to call it once
var hello = require('./custom_hello');
var gb = require('./custom_goodbye');

var multiFuncs = require('./custom_multiple_functions');

// console.log(hello);
hello();
// console.log(gb);
gb.goodbye();

multiFuncs.foo();
multiFuncs.bar();